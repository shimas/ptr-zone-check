# ptr-zone-check.go

ptr-zone-check is light golang module check BIND PTR zone records and see if those IPs still alive.

## Overview

ptr-zone-check.go: A golang module for tracking BIND PTR zone

## Build

    GOOS=linux GOARCH=amd64 go build ptr-zone-check.go

## LICENSE

Copyright 2017, shimas
