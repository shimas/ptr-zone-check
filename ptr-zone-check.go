/*
 * Track if BIND zone PTR records are still valid
 * https://www.nonamehosts.com/
 *
 * Copyright 2017, Darius Simanel
 *
 */

package main

import (
    "bufio"
    "fmt"
    "os"
    "regexp"
    "strings"
    "net"
)

func readZone(zoneFile string) {

	f, _ := os.Open(zoneFile)
    scanner := bufio.NewScanner(f)

    r, _ := regexp.Compile("PTR")
    n, _ := regexp.Compile("nonamehosts")
    i, _ := regexp.Compile("88.119.179")

    for scanner.Scan() {
        line := scanner.Text()
    	if r.MatchString(line) == true {
    		host := strings.SplitAfter(line, "PTR")
    		if n.MatchString(host[1]) != true {
    			//fmt.Println(strings.TrimSpace(host[1]))
    			if last := len(host[1]) - 1; last >= 0 && host[1][last] == '.' {
    				if hostip, err := net.LookupIP(strings.TrimSpace(host[1][:last])); err != nil {
    						fmt.Println(err)
    				} else {
    					if i.MatchString(hostip[0].String()) != true {
    						fmt.Println(strings.TrimSpace(host[1][:last]) + " : Resolves to outside IP: " + hostip[0].String())
						}
    				}
        		}
    		}
    	}
    }
    
    if err := scanner.Err(); err != nil {
        fmt.Fprintln(os.Stderr, "error:", err)
        os.Exit(1)
    }

}


func main() {
	
	if len(os.Args) < 2 {
		fmt.Println("Please enter PTR zone file")
	}else{
    	readZone(os.Args[1])
    }
}